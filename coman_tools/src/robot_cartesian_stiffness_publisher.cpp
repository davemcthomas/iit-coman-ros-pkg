#include <ros/ros.h>
#include <fcntl.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <tf/transform_listener.h>
#include <tf/tf.h>

#define freq 100

void printStiffnessMatrix(const double* K_C)
{
    std::cout<<K_C[0]<<"  "<<K_C[1]<<"  "<<K_C[2]<<std::endl;
    std::cout<<K_C[3]<<"  "<<K_C[4]<<"  "<<K_C[5]<<std::endl;
    std::cout<<K_C[6]<<"  "<<K_C[7]<<"  "<<K_C[8]<<std::endl;
}

void copyPCartesianStiffness(const double* KC_array, geometry_msgs::PoseWithCovarianceStamped& KC_msg)
{
    KC_msg.pose.covariance[0] = KC_array[0]; KC_msg.pose.covariance[1] = KC_array[1]; KC_msg.pose.covariance[2] = KC_array[2];
    KC_msg.pose.covariance[6] = KC_array[3]; KC_msg.pose.covariance[7] = KC_array[4]; KC_msg.pose.covariance[8] = KC_array[5];
    KC_msg.pose.covariance[12] = KC_array[6]; KC_msg.pose.covariance[13] = KC_array[7]; KC_msg.pose.covariance[14] = KC_array[8];
}

int main(int argc, char **argv)
{
    ROS_INFO("COMAN CARTESIAN STIFFNESS PUBLISHER NODE");

    //**** INITIALIZATION OF ROS, MSGS AND PUBLISHERS ****
    ros::init(argc, argv, "coman_cartesian_stiffness_publisher");
    ros::NodeHandle n;

    geometry_msgs::PoseWithCovarianceStamped KC_left_arm;
    geometry_msgs::PoseWithCovarianceStamped KC_right_arm;
    for(unsigned int i = 0; i < KC_left_arm.pose.covariance.size(); ++i)
    {
        KC_left_arm.pose.covariance[i] = 0.0;
        KC_right_arm.pose.covariance[i] = 0.0;
    }
    KC_left_arm.header.frame_id = "/l_wrist";
    KC_right_arm.header.frame_id = "/r_wrist";
    KC_left_arm.pose.pose.position.x = 0.0; KC_left_arm.pose.pose.position.y = 0.0; KC_left_arm.pose.pose.position.z = 0.0;
    KC_left_arm.pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0.0, 0.0, 0.0);
    KC_right_arm.pose.pose.position.x = 0.0; KC_right_arm.pose.pose.position.y = 0.0; KC_right_arm.pose.pose.position.z = 0.0;
    KC_right_arm.pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0.0, 0.0, 0.0);

    ros::Publisher Cartesian_Stiffness_left_arm_pub = n.advertise<geometry_msgs::PoseWithCovarianceStamped>("coman/CartesianStiffnessLeftArm", 1);
    ros::Publisher Cartesian_Stiffness_right_arm_pub = n.advertise<geometry_msgs::PoseWithCovarianceStamped>("coman/CartesianStiffnessRightArm", 1);

    ROS_INFO("OPENING xddp_sockets");
    int xddp_sock = -1;
    int xddp_sock2 = -1;
    do{
#if __XENO__
        xddp_sock = open("/proc/xenomai/registry/rtipc/xddp/CartesianStiffnessLeftArm", O_RDWR | O_NONBLOCK);
        xddp_sock2 = open("/proc/xenomai/registry/rtipc/xddp/CartesianStiffnessRightArm", O_RDWR | O_NONBLOCK);
#else
        xddp_sock = open("/tmp/CartesianStiffnessLeftArm", O_RDWR | O_NONBLOCK);
        xddp_sock2 = open("/tmp/CartesianStiffnessRightArm", O_RDWR | O_NONBLOCK);
#endif
        if (xddp_sock < 0 || xddp_sock2 < 0) {
            ROS_ERROR("error in _init: %s\n", strerror (errno));
            assert(errno);
            ROS_INFO("Try to reconnect in 1 sec...");
            sleep(1);
            }
    }while(xddp_sock < 0 || xddp_sock2 < 0);

    if(xddp_sock > 0){
        double   K_C_left_arm[9];
        double   K_C_right_arm[9];
        ROS_INFO("xddp_sockets OPEN");

        tf::TransformListener tf_listener;
        tf::StampedTransform T_left_arm;
        tf::StampedTransform T_right_arm;

        ROS_INFO("START PUBLISHING DATA");
        //**** GETTING DATA AND PUBLISH ****
        ros::Rate loop_rate(freq);
        int nbytes = 0;
        while(ros::ok())
        {
            do
            {
                nbytes = read(xddp_sock, (double*)&K_C_left_arm, 9*8);       
            } while (nbytes <= 0);
            if (nbytes < 0){
                continue;}
            copyPCartesianStiffness(K_C_left_arm, KC_left_arm);
            tf_listener.waitForTransform("base_link", "l_wrist", ros::Time(0),
                                         ros::Duration(1));
            tf_listener.lookupTransform("base_link", "l_wrist", ros::Time(0), T_left_arm);

            do
            {
                nbytes = read(xddp_sock2, (double*)&K_C_right_arm, 9*8);
            } while (nbytes == 0);
            if (nbytes < 0){
                continue;}
            copyPCartesianStiffness(K_C_right_arm, KC_right_arm);
            tf_listener.waitForTransform("base_link", "r_wrist", ros::Time(0),
                                         ros::Duration(1));
            tf_listener.lookupTransform("base_link", "r_wrist", ros::Time(0), T_right_arm);

//            KC_left_arm.header.stamp = T_left_arm.stamp_;
//            KC_right_arm.header.stamp = T_right_arm.stamp_;
            KC_left_arm.header.stamp = ros::Time::now();
            KC_right_arm.header.stamp = KC_left_arm.header.stamp;

            Cartesian_Stiffness_left_arm_pub.publish(KC_left_arm);
            Cartesian_Stiffness_right_arm_pub.publish(KC_right_arm);

            ros::spinOnce();
            //loop_rate.sleep();
        }

    }

    close(xddp_sock);
    close(xddp_sock2);
    ROS_INFO("xddp socket closed");

	return 0;
}
