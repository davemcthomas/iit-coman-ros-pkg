#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/JointState.h"
#include <openrave-core.h>

#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>
#include <math.h>

#include <tf/transform_broadcaster.h>


using namespace OpenRAVE;

RobotBasePtr probot;
std::vector<RobotBasePtr> vrobots;
ViewerBasePtr viewer;
std::vector<GraphHandlePtr> handles;
std::string viewername = "qtcoin";
std::string scenefilename = "../COMAN/environment_NoPhisics.xml";




void wam_joints_Callback(const sensor_msgs::JointState::ConstPtr& msg)
{
    static tf::TransformBroadcaster br;
    tf::Transform transform;

    OpenRAVE::Transform T;

    ROS_DEBUG("I heard: [%.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f]", msg->position[0], msg->position[1], msg->position[2], msg->position[3], msg->position[4], msg->position[5], msg->position[6], msg->position[7], msg->position[8], msg->position[9], msg->position[10], msg->position[11], msg->position[12], msg->position[13], msg->position[14], msg->position[15], msg->position[16], msg->position[17], msg->position[18], msg->position[19], msg->position[20], msg->position[21], msg->position[22]);
    std::vector<dReal> joints;
    std::vector<int> index_joint;
    index_joint.push_back(0);index_joint.push_back(1);index_joint.push_back(2);
    index_joint.push_back(3);index_joint.push_back(4);index_joint.push_back(5);
    index_joint.push_back(6);
    index_joint.push_back(7);index_joint.push_back(8);index_joint.push_back(9);
    index_joint.push_back(10);index_joint.push_back(11);index_joint.push_back(12);
    index_joint.push_back(13);
    index_joint.push_back(14);index_joint.push_back(15);index_joint.push_back(16);
    index_joint.push_back(17);index_joint.push_back(18);index_joint.push_back(19);
    index_joint.push_back(20);index_joint.push_back(21);index_joint.push_back(22);


    // WAIST
    joints.push_back( msg->position[2] );joints.push_back(msg->position[1] ); joints.push_back( msg->position[0] );

    // RIGHT LEG
    joints.push_back( msg->position[3] );joints.push_back( msg->position[5] );joints.push_back( msg->position[6] );
    joints.push_back( msg->position[7] );joints.push_back( msg->position[9] );joints.push_back( msg->position[8] );

    //LEFT LEG
    joints.push_back( msg->position[4] );joints.push_back( msg->position[10] );joints.push_back( msg->position[11] );
    joints.push_back( msg->position[12] );joints.push_back( msg->position[14] );joints.push_back( msg->position[13] );

    //RIGHT ARM
    joints.push_back( msg->position[15] ); joints.push_back( msg->position[16]);joints.push_back( msg->position[17] );joints.push_back( msg->position[18] );

    //LEFT ARM
    joints.push_back( msg->position[19]  );joints.push_back( msg->position[20]);joints.push_back( msg->position[21] ); joints.push_back( msg->position[22] );

    probot->SetDOFValues(joints,1,index_joint);





    // WAIST
    T = probot->GetLink("Waist")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/Waist"));

    T = probot->GetLink("DWS")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/DWS"));

    T = probot->GetLink("DWL")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/DWL"));

    T = probot->GetLink("DWYTorso")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/DWYTorso"));



    //RIGHT LEG
    T = probot->GetLink("RHipMot")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/RHipMot"));

    T = probot->GetLink("RThighUpLeg")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/RThighUpLeg"));

    T = probot->GetLink("RThighLowLeg")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/RThighLowLeg"));

    T = probot->GetLink("RLowLeg")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/RLowLeg"));

    T = probot->GetLink("RFootmot")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/RFootmot"));

    T = probot->GetLink("RFoot")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/RFoot"));



    //LEFT LEG
    T = probot->GetLink("LHipMot")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/LHipMot"));

    T = probot->GetLink("LThighUpLeg")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/LThighUpLeg"));

    T = probot->GetLink("LThighLowLeg")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/LThighLowLeg"));

    T = probot->GetLink("LLowLeg")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/LLowLeg"));

    T = probot->GetLink("LFootmot")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/LFootmot"));

    T = probot->GetLink("LFoot")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/LFoot"));



    //RIGHT ARM
    T = probot->GetLink("RShp")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/RShp"));

    T = probot->GetLink("RShr")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/RShr"));

    T = probot->GetLink("RShy")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/RShy"));

    T = probot->GetLink("RElb")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/RElb"));



    //LEFT ARM
    T = probot->GetLink("LShp")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/LShp"));

    T = probot->GetLink("LShr")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/LShr"));

    T = probot->GetLink("LShy")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/LShy"));

    T = probot->GetLink("LElb")->GetTransform();
    transform.setOrigin( tf::Vector3(T.trans.x, T.trans.y, T.trans.z) );
    transform.setRotation( tf::Quaternion(T.rot.x, T.rot.y, T.rot.z, T.rot.w ) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/world", "/coman/LElb"));

}

void SetViewer(EnvironmentBasePtr penv, const std::string& viewername)
{
    ViewerBasePtr viewer = RaveCreateViewer(penv,viewername);
    BOOST_ASSERT(!!viewer);

    // attach it to the environment:
    penv->Add(viewer);

    // finally call the viewer's infinite loop (this is why a separate thread is needed)
    bool showgui = true;
    viewer->main(showgui);
}



int main(int argc, char **argv)
{

        RaveInitialize(true); // start openrave core
        EnvironmentBasePtr penv = RaveCreateEnvironment(); // create the main environment
        RaveSetDebugLevel(Level_Debug);


        boost::thread thviewer(boost::bind(SetViewer,penv,viewername));
        penv->Load(scenefilename); // load the scene

        penv->GetRobots(vrobots);
        probot = vrobots.at(0);


        ros::init(argc, argv, "listener");

        ros::NodeHandle n("coman_observer_openrave");

        ros::Subscriber sub = n.subscribe("/coman/joints_state", 1, wam_joints_Callback);

        ros::spin();

        thviewer.join(); // wait for the viewer thread to exit
        penv->Destroy(); // destroy



	return 0;
}



